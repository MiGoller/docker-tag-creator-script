# Docker Tag Creator Script
This repository provides simple scripts to create a list of tags based on multiple inputs to tag Docker images on build.

## The purpose of the scripts
The purpose of the scripts is to provide a simple and standarized method to create tags for Docker images on different contributing factors.The script should become a reusable module for CI/CD-pipelines and build-scripts.

## How to get the scripts?
There are many ways to incorporate the scripts to your build processes. You could clone the repository or download the single files you need, but I recommend to incorporated the latest artifacts of the scripts. The build pipelines push script artifacts only if the tests will succeed.

You can download the artifacts or incorporate them even at build-time using these links.
- [createTagList.sh](https://gitlab.com/MiGoller/docker-tag-creator-script/-/jobs/artifacts/master/raw/createTagList.sh?job=deploy-artifacts-master) is the core script.
- [buildDockerImageDockerHub.sh](https://gitlab.com/MiGoller/docker-tag-creator-script/-/jobs/artifacts/master/raw/buildDockerImageDockerHub.sh?job=deploy-artifacts-master) runs `createTagList.sh` to build and tag an image and to push it to Docker Hub.
- [buildDockerImageDryRun.sh](https://gitlab.com/MiGoller/docker-tag-creator-script/-/jobs/artifacts/master/raw/buildDockerImageDryRun.sh?job=deploy-artifacts-master) runs `createTagList.sh` to show you the generated tags.
- [buildDockerImageGitLab.sh](https://gitlab.com/MiGoller/docker-tag-creator-script/-/jobs/artifacts/master/raw/buildDockerImageGitLab.sh?job=deploy-artifacts-master) runs `createTagList.sh` to build and tag an image and to push it to your GitLab registry.

The [complete set of scripts as an archive](https://gitlab.com/MiGoller/docker-tag-creator-script/-/jobs/artifacts/master/download?job=deploy-artifacts-master) is availabe as latest artifact as well.

## Contributing factors
The script supports these contribution factors:

| Parameter | Name             | Description |
|-----------|------------------|-------------|
|     1     | `FLAVOR`           | Set to tag images for different variants like "official", "iot", whatever you want. |
|     2     | `VERSION`          | The application's version information. If you set an `-` in front of the version number, no tags for major and minor releases get generated. Set to `false` if you do not want any tags generated for the application's version.|
|     3     | `SPECIAL_VERSIONS` | Add these comma-seperated version information to the list of app's version based tag list. |
|     4     | `ISDEFAULT`        | Set to true, if you want to include default tags like the version information only. |
|     5     | `SPECIAL_TAGS`     | Add these comma-seperated tags like "latest","stable". |
|     6     | `SUFFIX`           | Add a suffix to each APP_FLAVOR based tag, but not to the default tags. |
|     7     | `DISTRO`           | Subfolder containing the Dockerfile. Not used for creating the tag list. |

## Expected tagging list
The script should be able to produce a list with tags caring about the app's version, special tags, special version, special versions and suffixes.

This is an almost fully generate tag list. `buildDockerImage.sh` executes the script `creatTagList.sh` to get the corresponding tags.
```
$ ./buildDockerImageGitLab.sh \ 
    "[FLAVOR]" \
    "1.2.3" \
    "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" \
    "true" \
    "[SPECIAL_TAG1],[SPECIAL_TAG2]" \
    "[SUFFIX]" \
    "DISTRO"
```

  - `[SPECIAL_TAG1]`
  - `[SPECIAL_TAG2]`
  - `1`
  - `1.2`
  - `1.2.3`
  - `[SPECIAL_VERSION1]`
  - `[SPECIAL_VERSION2]`
  - `[FLAVOR]` `[SUFFIX]`
  - `1`-`[FLAVOR]` `[SUFFIX]`
  - `1.2`-`[FLAVOR]` `[SUFFIX]`
  - `1.2.3`-`[FLAVOR]` `[SUFFIX]`
  - `[SPECIAL_VERSION1]`-`[FLAVOR]` `[SUFFIX]`
  - `[SPECIAL_VERSION2]`-`[FLAVOR]` `[SUFFIX]`
 
# The scripts
The repository contains one script to create the tag list. Another script will build Docker images for GitLab CI/CD-pipelines using the the script to create the tag list.

## Creating the tag list
The script to create the tag list is called `createTagList.sh`. Pass the seven contributing factors to the script as commandline parameters in doublequotes.

```
$ ./createTagList.sh \
    "[FLAVOR]" \
    "1.2.3" \
    "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" \
    "true" \
    "[SPECIAL_TAG1],[SPECIAL_TAG2]" \
    "[SUFFIX]" \
    "DISTRO"
```

Executing the script will generate an `export` command we can `source` later.

```
export TAG_LIST=[SPECIAL_TAG1],[SPECIAL_TAG2],1,1.2,1.2.3,[SPECIAL_VERSION1],[SPECIAL_VERSION2],[FLAVOR][SUFFIX],1-[FLAVOR][SUFFIX],1.2-[FLAVOR][SUFFIX],1.2.3-[FLAVOR][SUFFIX],[SPECIAL_VERSION1]-[FLAVOR][SUFFIX],[SPECIAL_VERSION2]-[FLAVOR][SUFFIX]
```

## Building and tagging Docker images
There are two scripts to build and tag Docker images for your Docker project on GitLab. 
- The first one will push Docker images to your GitLab repository's registry. This is the default way.
- The second script will push your Docker images to [Docker Hub](https://hub.docker.com/).

Pass the seven contributing factors to the scripts as commandline parameters in doublequotes. These scripts call `createTagList.sh` to get the tag list for tagging your Docker image.

> You will have to add `bash` to the Alpine-linux based [DIND Images](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html) on GitLab to run the scripts! See example.

### Building, tagging Docker images and push to GitLab
Let your GitLab CI/CD run the script `buildDockerImageGitLab.sh` to build and tag your Docker image. Finally the scripts pushes it to your GitLab registry.

```
$ ./buildDockerImageGitLab.sh \ 
    "[FLAVOR]" \
    "1.2.3" \
    "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" \
    "true" \
    "[SPECIAL_TAG1],[SPECIAL_TAG2]" \
    "[SUFFIX]" \
    "DISTRO"
```

### Building, tagging Docker images and push to Docker Hub
Well, that's almost the same as running the script to push the Docker image to the GitLab registry. Run the script `buildDockerImageDockerHub.sh` to build and tag your Docker image. Finally the script pushes it to your [Docker Hub](https://hub.docker.com/) registry.


```
$ ./buildDockerImageDockerHub.sh \ 
    "[FLAVOR]" \
    "1.2.3" \
    "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" \
    "true" \
    "[SPECIAL_TAG1],[SPECIAL_TAG2]" \
    "[SUFFIX]" \
    "DISTRO"
```

#### Setting the CI/CD-variables for Docker Hub
To be able to push to the [Docker Hub](https://hub.docker.com/) registry you have to set these pipeline variables like the `$CI_REGISTRY_` variables but to address the [Docker Hub](https://hub.docker.com/) registry.

> Set these variables at your GitLab CI/CD-settings as [protected variables](https://gitlab.com/help/ci/variables/README#variables).

| Docker Hub | Description |
|-----------|------------------|
| `DH_REGISTRY` | Set to `docker.io` for Docker Hub. |
| `DH_REGISTRY_USER` | Your username on Docker Hub. |
| `DH_REGISTRY_PASSWORD` | Your password on Docker Hub. |
| `DH_REGISTRY_IMAGE` | Set to `docker.io/[Your Docker Hub username]/[Name of your Docker Hub repository to push to` |

## GitLab CI/CD example
This is just a simple GitLAB CI/CD example to show you how to add the scripts to your build pipelines.

Adjust your `.gitlab-ci.yml` file to accomplish that.

```
image: docker:latest

services:
  - docker:dind

before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  - export BUILD_DATE=$(date +"%Y.%m.%d %H:%M:%S")

build-dev:
  stage: build
  script:
    - docker build --pull --build-arg ARG_APP_VERSION="DEV" --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA --build-arg ARG_BUILD_DATE="$BUILD_DATE" -t "$CI_REGISTRY_IMAGE:dev-latest" .
    - docker push "$CI_REGISTRY_IMAGE:dev-latest"
  only:
    - dev

build-master:
  stage: build
  script:
    - apk add --update --no-cache bash
    - /bin/bash ./buildDockerImageGitLab.sh "[FLAVOR]" "1.2.3" "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" "true" "[SPECIAL_TAG1],[SPECIAL_TAG2]" "[SUFFIX]" "DISTRO"
    - /bin/bash ./buildDockerImageDockerHub.sh "[FLAVOR]" "1.2.3" "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" "true" "[SPECIAL_TAG1],[SPECIAL_TAG2]" "[SUFFIX]" "DISTRO"
  only:
    - master

```

Good luck and enjoy.

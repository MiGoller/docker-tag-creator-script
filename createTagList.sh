#!/bin/bash

set -e

#   ./createTagList.sh APP_FLAVOR APP_VERSION APP_SPECIAL_VERSIONS APP_ISDEFAULT APP_SPECIAL_TAGS APP_SUFFIX APP_DISTRO "lockedTag1, lockedTag2, ..."
#   Official release:   ./createTagList.sh "official" "1.2.3" "" "false" "latest" "" "official"
#   Alpine release:     ./createTagList.sh "alpine" "1.2.3" "" "false" "" "" "alpine"
#   Debian release:     ./createTagList.sh "debian" "1.2.3" "" "false" "" "" "debian"

###############################################################################
#   Build list of tags for the image.
###############################################################################

containsElement () {
    local e match="$1"
    shift
    for e; do [[ "$e" == "$match" ]] && return 0; done
    return 1
}

# The flavor tag
APP_FLAVOR="${1}"

# Split APP_VERSION for naming the tags
APP_VERSION_STAGE="${2}"

# Do not create major, minor and release tags if the version number starts with "-"
if [ $APP_VERSION_STAGE == "false" ]; then
    # Do not tag any version information
    MY_VERSION=(  )
    APP_VERSION=""
else
    if [[ $APP_VERSION_STAGE == \-* ]]; then
        MY_VERSION=(  )
        APP_VERSION=${APP_VERSION_STAGE:1}
    else
        APP_VERSION=${APP_VERSION_STAGE}
        MY_VERSION=( ${APP_VERSION//./ } )
    fi
fi

# Special versions?
APP_SPECIAL_VERSIONS="${3}"
sversions=( ${APP_SPECIAL_VERSIONS//,/ } )

# Is default image type?
APP_ISDEFAULT="${4}"

# Any special tags?
APP_SPECIAL_TAGS="${5}"
stags=( ${APP_SPECIAL_TAGS//,/ } )

# Suffix
APP_SUFFIX="${6}"

# APP_FLAVOR + APP_SUFFIX
APP_FLAVOR_S=${APP_FLAVOR}

if [ -n "${APP_SUFFIX}" ]; then
    APP_FLAVOR_S="${APP_FLAVOR_S}${APP_SUFFIX}"
fi

# Node version
APP_DISTRO="${7}"

# Locked tags
APP_LOCKED_TAGS="${8}"
lockedTags=( ${APP_LOCKED_TAGS//,/ } )

### Build tag array
tags=()

# Add special tags
if [ -n "${APP_SPECIAL_TAGS}" ]; then
    for tag in "${stags[@]}"; do
        tags+=( ${tag} )
    done
fi

# Add tags for version for default image type only!
if [[ "${APP_ISDEFAULT}" == "true" ]] || [[ (( ${#APP_FLAVOR_S} -lt 1 )) && (( ${#APP_FLAVOR} -lt 1 )) ]]; then
    if [ -n "${APP_VERSION}" ]; then
        if [ ${#MY_VERSION[@]} -ge 1 ]; then
            for tag in {"${MY_VERSION[0]}","${MY_VERSION[0]}.${MY_VERSION[1]}"}; do
                tags+=( ${tag} )
            done
        fi

        tags+=( ${APP_VERSION} )
    fi

    # Any special version?
    if [ -n "${APP_SPECIAL_VERSIONS}" ]; then
        for tag in "${sversions[@]}"; do
            tags+=( ${tag} )
        done
    fi
fi

# Add tags for version AND flavor
if [ -n "${APP_FLAVOR_S}" ]; then
    if [ -n "${APP_FLAVOR}" ]; then
        tags+=( ${APP_FLAVOR_S} )
    fi

    if [ -n "${APP_VERSION}" ]; then
        if [ ${#MY_VERSION[@]} -ge 1 ]; then
            for tag in {"${MY_VERSION[0]}-${APP_FLAVOR_S}","${MY_VERSION[0]}.${MY_VERSION[1]}-${APP_FLAVOR_S}"}; do
                tags+=( ${tag} )
            done
        fi

        tags+=( ${APP_VERSION}-${APP_FLAVOR_S} )
    fi

    # Any special version?
    if [ -n "${APP_SPECIAL_VERSIONS}" ]; then
        for tag in "${sversions[@]}"; do
            tags+=( ${tag}-${APP_FLAVOR_S} )
        done
    fi
fi

# List all tags
EXPORT_TAGS=""
for tag in "${tags[@]}"; do
    # Check for forbidden tags
    if ! containsElement "${tag}" "${lockedTags[@]}"; then 
        if [ -n "${EXPORT_TAGS}" ]; then
            EXPORT_TAGS="${EXPORT_TAGS},${tag}"
        else
            EXPORT_TAGS="${tag}"
        fi
    fi
done

# Export the taglist as an environment variable
echo "export TAG_LIST=${EXPORT_TAGS}"

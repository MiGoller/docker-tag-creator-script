#!/bin/bash

set -e

#   ./releaseDockerImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "lockedTag1, lockedTag2, ..."
#   Official release:   ./releaseDockerImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "official" "1.2.3" "" "false" "latest" "" "official"
#   Alpine release:     ./releaseDockerImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "alpine" "1.2.3" "" "false" "" "" "alpine"
#   Debian release:     ./releaseDockerImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "debian" "1.2.3" "" "false" "" "" "debian"

###############################################################################
#   Mandatory commandline arguments:
#       SOURCE_IMAGE_NAME       :   The name of the already pulled or built image to tag and to push.
#       DEST_IMAGE_NAME         :   The name of the destination image WITHOUT ANY TAG
#       APP_FLAVOR              :   Set to tag images for different variants like "official", "iot", whatever you want.
#       APP_VERSION             :   Set to the application's version information
#       APP_SPECIAL_VERSIONS    :   Add these comma-seperated version information to the list of app's version based tag list
#       APP_ISDEFAULT           :   Set to true, if you want to include default tags like the version information only.
#       APP_SPECIAL_TAGS        :   Add these comma-seperated tags like "latest","stable"
#       APP_SUFFIX              :   Add a suffix to each APP_FLAVOR based tag, but not to the default tags.
#       APP_DISTRO              :   Subfolder containing the Dockerfile
#       [lockedTags list]       :   Comma-seperated list of locked tags
###############################################################################

###############################################################################
#   Build, tag and push the image.
###############################################################################
echo "-------------------------------------------------------------------------------"
echo "Parsing commandline parameters:"

export SOURCE_IMAGE_NAME=$1
export DEST_IMAGE_NAME=$2
export APP_FLAVOR=$3
export APP_VERSION=$4
export APP_SPECIAL_VERSIONS=$5
export APP_ISDEFAULT=$6
export APP_SPECIAL_TAGS=$7
export APP_SUFFIX=$8
export APP_DISTRO=$9
export APP_LOCKED_TAGS="${10}"

if [ -z "$BUILD_DATE" ]; then
    export BUILD_DATE=$(date +"%Y.%m.%d %H:%M:%S")
    echo " - Build date (set): $BUILD_DATE"
fi

echo " - Source image:      $SOURCE_IMAGE_NAME"
echo " - Destination image: $DEST_IMAGE_NAME"
echo " - Flavor:            $APP_FLAVOR"
echo " - Version:           $APP_VERSION"
echo " - Special versions:  $APP_SPECIAL_VERSIONS"
echo " - Is default:        $APP_ISDEFAULT"
echo " - Special tags:      $APP_SPECIAL_TAGS"
echo " - Tag suffix:        $APP_SUFFIX"
echo " - Subfolder:         $APP_DISTRO"
echo " - Locked tags:       $APP_LOCKED_TAGS"
echo ""

echo "Generating tags run createTagList.sh:"
eval "$( ./createTagList.sh "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO" "$APP_LOCKED_TAGS")"

echo " - $TAG_LIST"
echo ""

echo "Tagging the source image: $SOURCE_IMAGE_NAME ..."

tags=( ${TAG_LIST//,/ } )

for tag in "${tags[@]}"; do
    # Tag and push image for each tag in list
    echo "  - $DEST_IMAGE_NAME:${tag} ..."
    docker tag "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME:${tag}"
done

echo "Pushing image and tags:"
for tag in "${tags[@]}"; do
    echo "  - $DEST_IMAGE_NAME:${tag} ..."
    docker push "$DEST_IMAGE_NAME:${tag}"
done

echo "-------------------------------------------------------------------------------"
echo "Building and tagging sequence:"
for tag in "${tags[@]}"; do
    echo "  - $DEST_IMAGE_NAME:${tag} "
done

echo "-------------------------------------------------------------------------------"
echo "Release succeeded!"

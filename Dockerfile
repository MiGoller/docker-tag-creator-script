FROM alpine

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="GPLv3" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.description="Docker image for CI/CD tests" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/docker-tag-creator-script.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Create additional directories for: Custom configuration, working directory, database directory, scripts
RUN mkdir -p /opt/dockertagcreator

# Assign working directory
WORKDIR /opt/dockertagcreator

# Install package dependencies
RUN apk update && \ 
    apk add --no-cache bash

# Copy code
COPY buildDockerImage*.sh ./
COPY createTagList.sh ./

RUN chmod +x *.sh

CMD /bin/bash -C '/opt/dockertagcreator/buildDockerImageDryRun.sh' "[FLAVOR]" "1.2.3" "[SPECIAL_VERSION1],[SPECIAL_VERSION2]" "true" "[SPECIAL_TAG1],[SPECIAL_TAG2]" "[SUFFIX]" "[DISTRO]"
